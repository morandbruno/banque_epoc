function Verso(element,numero){
    document.getElementById(numero).getElementsByClassName('resume')[0].style.display="block";
    document.getElementById(numero).getElementsByClassName('qrcode')[0].style.display="none";
    document.getElementById(numero).style.transform='rotateY(180deg)';
}

function Recto(element,numero){
    document.getElementById(numero).style.transform='rotateY(0deg)';
}


function search_epoc() {
    let input = document.getElementById('searchbar').value
    input=input.toLowerCase();
    let x = document.getElementsByClassName('filtre');
      
    for (i = 0; i < x.length; i++) { 
        if (!x[i].innerHTML.toLowerCase().includes(input)) {
            console.log(x[i].parentElement.parentElement);
            x[i].parentElement.parentElement.style.display="none";
        }
        else {
            console.log(x[i].parentElement.parentElement);
            x[i].parentElement.parentElement.style.display="block";                 
        }
    }
}

function myQR(element,numero){
    document.getElementById(numero).getElementsByClassName('resume')[0].style.display="none";
    document.getElementById(numero).getElementsByClassName('qrcode')[0].style.display="block";
    document.getElementById(numero).style.transform='rotateY(180deg)';
}